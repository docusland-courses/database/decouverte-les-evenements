# Gestion d'évènements au sein de MySQL

Mysql a intégré une fonctionnalité d'évènements. 
Les évènements permettent d'éviter devoir planifier des cron ou des taches planifiées pour l'automatisation de l'execution de tâches SQL. 

Il faut penser les évènements comme des schedulers de procédures stockées.

Afin de vous assurer que les évènements vont bien se déclencher, il est nécessaire d'activer le gestionnaire d'évèments global. 

```sql
SHOW VARIABLES WHERE variable_name='event_scheduler';
-- or
SELECT @@event_scheduler
```

Si vous voyez la valeur `OFF`, aucun évènement ne sera déclenché. Vous devez alors réaliser :
```sql
SET GLOBAL event_scheduler="ON"
```

# Exercice

```sql
CREATE TABLE Message
(   id INT AUTO_INCREMENT PRIMARY KEY,
    userId INT NOT NULL,
    message VARCHAR(255) NOT NULL,
    updateDt DATETIME NOT NULL,
    KEY(updateDt)
);
 
INSERT Message(userId,message,updateDt) VALUES (1,'message 123','2015-08-24 11:10:09');
INSERT Message(userId,message,updateDt) VALUES (7,'message 124','2015-08-29');
INSERT Message(userId,message,updateDt) VALUES (1,'message 125','2015-09-03 12:00:00');
INSERT Message(userId,message,updateDt) VALUES (1,'message 126','2015-09-03 14:00:00');
```

## Créer un évènement Delete7DaysOldMessage

Créer un évènement, ce dernier doit se déclencher tous les jours. Il doit supprimer les messages ayant plus de 7 jours. 


## Créer un évènement  Delete10MinutesOldMessage
Même logique mais cet évènement doit se déclencher toutes les minutes.

## Lister les évènements présents dans une base de données
## Supprimer les évènements précédement générés
